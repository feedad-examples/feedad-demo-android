# FeedAd Android SDK Demo App

This application shows how to configure and integrate the FeedAd Android SDK into an Android App.


## Getting started

If you want to run the application you have to set your client token first:

1. Copy the **native** client token from your FeedAd dashboard, or your onboarding email.
2. Paste in into `app/src/main/res/values/config_feedad.xml` as the value of `feedad_client_token`.


## App Structure

The app consists of a single activity that features a navigation drawer menu.
When selected, each menu item will replace the main content frame of the activity with a fragment
showing a different kind of integration method of the SDK.

There are code comments inside each file that describe the integration. For in-depth explanations
of the FeedAd SDK please refer to the SDK's documentation page. 


### SDK Initialization

The following files show how to integrate & initialize the FeedAd SDK:
- `app/build.gradle`: SDK repository and dependency notation.
- `app/src/main/java/.../App.java`: SDK initialization
- `app/src/main/res/values/config_feedad.xml`: SDK configuration


### Ad requests

The following files show how to include FeedAds into your layout and how to request an ad:
- `app/src/.../SimpleIntegrationFragment.java`: The simplest form of integration by just adding a view to your layout.
- `app/src/.../ScrollViewIntegrationFragment.java`: Demonstrates that a FeedAdView also works within a classic scroll view.
- `app/src/.../RecyclerViewIntegrationFragment.java`: Shows how to integrate FeedAd into a heterogeneous recycler view.
- `app/src/.../AdvancedIntegrationFragment.java`: Shows how to integrate FeedAd based on Listener responses.


## FeedAd Mediation examples

The demo app includes examples of custom mediation using DFP, AppLovin MAX and AdMob.
If you want to run those examples within the demo app your need to configure each framework to serve ads to the demo app.


### AppLovin Configuration

Before this example will work, you will have to configure your FeedAd client token. (See "Getting Started")

The app shows how to use FeedAd with AppLovin banner, interstitials, and native ads.
Look at the following files to see the different parts of the integration:

- `app/build.gradle`: The dependencies for AppLovin and the FeedAd AppLovin plugin
- `AppLovinMAXIntegrationFragment.java`: The example code that shows how to integrate FeedAd into AppLovin.
- `config_applovin.xml`: The ad unit IDs for AppLovin.

Read the [documentation](https://docs.feedad.com/android/mediation_networks/applovin/) for examples on how to configure FeedAd in AppLovin's dashboard.

### Google Ads Configuration

Before this example will work, you will have to configure your FeedAd client token. (See "Getting Started")

The app shows how to use FeedAd with banner and interstitials ads served by Google Ad Manager or Google AdMob.
Look at the following files to see the different parts of the integration:

- `app/build.gradle`: The dependencies for Google Ads and the FeedAd Google Ads plugin
- `GoogleAdManagerIntegrationFragment.java`: The example code that shows how to integrate FeedAd into Google Ads.
- `config_google_ads.xml`: The ad unit IDs for Google Ads.

Read the [documentation](https://docs.feedad.com/android/mediation_networks/google_ads/) for a more detailed description.