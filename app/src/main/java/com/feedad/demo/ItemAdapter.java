package com.feedad.demo;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * This adapter can display two types of items: Dummy content and FeedAds
 */
class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.BaseViewHolder> {

    private static final int VIEW_TYPE_CONTENT = 0;
    private static final int VIEW_TYPE_FEED_AD = 1;

    private final List<ListItem> items;

    public ItemAdapter() {
        items = new ArrayList<>();
    }

    public ItemAdapter(List<ListItem> items) {
        this.items = items;
    }

    public void add(int index, ListItem item) {
        items.add(index, item);
        notifyItemInserted(3);
    }

    public <T extends ListItem> void removeAllOfType(Class<T> itemClass) {
        List<Integer> targets = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            ListItem item = items.get(i);
            if (itemClass.isInstance(item)) {
                targets.add(i);
            }
        }
        for (int i = 0; i < targets.size(); i++) {
            int removeIdx = targets.get(i) - i;
            items.remove(removeIdx - i);
            notifyItemRemoved(removeIdx - i);
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_CONTENT:
                return new ContentItemHolder(parent);
            case VIEW_TYPE_FEED_AD:
                return new FeedAdItemHolder(parent);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Item contract to specify if the item is content or an ad.
     */
    interface ListItem {

        int getViewType();
    }

    /**
     * An item for dummy content
     */
    static class ContentItem implements ListItem {

        @Override
        public int getViewType() {
            return VIEW_TYPE_CONTENT;
        }

    }

    /**
     * An item for FeedAds
     */
    static class FeedAdItem implements ListItem {

        @Override
        public int getViewType() {
            return VIEW_TYPE_FEED_AD;
        }
    }

    /**
     * Basic viewHolder class as type of this adapter
     */
    abstract class BaseViewHolder extends RecyclerView.ViewHolder {

        BaseViewHolder(View parent) {
            super(parent);
        }
    }

    /**
     * View holder for the dummy content
     */
    private class ContentItemHolder extends BaseViewHolder {

        ContentItemHolder(View parent) {
            super(LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_content, (ViewGroup) parent, false));
        }

    }

    /**
     * View holder for the ad view
     */
    private class FeedAdItemHolder extends BaseViewHolder {

        FeedAdItemHolder(View parent) {
            super(LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_feedad_view, (ViewGroup) parent, false));
        }

    }
}