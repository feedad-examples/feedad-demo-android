package com.feedad.demo.applovin;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.mediation.nativeAds.MaxNativeAdListener;
import com.applovin.mediation.nativeAds.MaxNativeAdLoader;
import com.applovin.mediation.nativeAds.MaxNativeAdView;
import com.applovin.mediation.nativeAds.MaxNativeAdViewBinder;
import com.feedad.android.FeedAdView;
import com.feedad.android.applovin.FeedAdMaxNativeAdView;
import com.feedad.demo.R;
import com.feedad.demo.databinding.FeedadApplovinBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.ObservableBoolean;
import androidx.fragment.app.Fragment;

/**
 * This fragment shows how to load FeedAd through AppLovin MAX for MREC, native, and interstitial ads.
 */
public class AppLovinMAXIntegrationFragment extends Fragment {

    private ObservableBoolean isInterstitialRequesting;

    @Nullable private MaxInterstitialAd interstitialAd = null;
    @Nullable private MaxNativeAdLoader nativeAdLoader = null;
    @Nullable private MaxAd nativeAd = null;
    private FeedadApplovinBinding layout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        isInterstitialRequesting = new ObservableBoolean(false);
        layout = FeedadApplovinBinding.inflate(inflater, container, false);
        layout.setInterstitialRequesting(isInterstitialRequesting);
        return layout.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        layout.btnRequestBanner.setOnClickListener(v -> requestBannerAd());
        layout.btnRequestNative.setOnClickListener(v -> requestNativeAd());
        layout.btnRequestInterstitial.setOnClickListener(v -> requestInterstitial());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (nativeAdLoader != null) {
            if (nativeAd != null) {
                nativeAdLoader.destroy(nativeAd);
            }
            nativeAdLoader.destroy();
        }
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
    }

    private void requestBannerAd() {
        clearAdContainer();
        MaxAdView adView = new MaxAdView(getString(R.string.applovin_banner_unit_id), MaxAdFormat.MREC, requireActivity());
        layout.adContainer.addView(adView);
        adView.loadAd();
    }

    private void requestNativeAd() {
        clearAdContainer();
        // create native ad loader
        nativeAdLoader = new MaxNativeAdLoader(getString(R.string.applovin_native_unit_id), requireActivity());

        // add native ad to layout if there is fill, and log an error if not
        nativeAdLoader.setNativeAdListener(new MaxNativeAdListener() {
            @Override
            public void onNativeAdLoaded(MaxNativeAdView maxNativeAdView, MaxAd maxAd) {
                if (nativeAd != null) {
                    nativeAdLoader.destroy(nativeAd);
                }
                nativeAd = maxAd;
                layout.adContainer.removeAllViews();
                layout.adContainer.addView(maxNativeAdView);
            }

            @Override
            public void onNativeAdLoadFailed(String s, MaxError maxError) {
                Toast.makeText(requireActivity(), String.format("Native ad failed: %s, %s", s, maxError), Toast.LENGTH_LONG).show();
            }
        });

        // bind the custom native ad layout
        MaxNativeAdViewBinder binder = new MaxNativeAdViewBinder.Builder(R.layout.native_custom_ad_view)
                .setTitleTextViewId(R.id.title_text_view)
                .setBodyTextViewId(R.id.body_text_view)
                .setAdvertiserTextViewId(R.id.advertiser_textView)
                .setIconImageViewId(R.id.icon_image_view)
                .setMediaContentViewGroupId(R.id.media_view_container)
                .setOptionsContentViewGroupId(R.id.ad_options_view)
                .setCallToActionButtonId(R.id.cta_button)
                .build();

        // create the ad view using the FeedAdMaxNativeAdWrapper
        MaxNativeAdView nativeAdView = new FeedAdMaxNativeAdView(binder, requireActivity()) {
            @Override
            public void layoutForFeedAd(@NonNull FeedAdView feedAdView) {
                // if the ad is a FeedAd this method will be invoked
                // adjust the native layout accordingly

                // this example will hide all views except the mediaContentViewGroup, and enlarge this group
                ViewGroup mediaContentViewGroup = getMediaContentViewGroup();
                ViewGroup.LayoutParams params = mediaContentViewGroup.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                mediaContentViewGroup.setLayoutParams(params);

                if (getMainView() instanceof ViewGroup) {
                    ViewGroup mainView = (ViewGroup) getMainView();
                    for (int i = 0; i < mainView.getChildCount(); i++) {
                        if (mainView.getChildAt(i) != mediaContentViewGroup) {
                            mainView.getChildAt(i).setVisibility(GONE);
                        }
                    }
                }

                // set additional layout options on the FeedAdView if you want to
                feedAdView.setShutterDrawable(new ColorDrawable(ResourcesCompat.getColor(getResources(), R.color.feedAdShutter, requireActivity().getTheme())));
            }
        };
        nativeAdLoader.loadAd(nativeAdView);
    }

    private void clearAdContainer() {
        layout.adContainer.removeAllViews();
    }

    private void requestInterstitial() {
        if (isInterstitialRequesting.get()) {
            return;
        }
        isInterstitialRequesting.set(true);
        interstitialAd = new MaxInterstitialAd(getString(R.string.applovin_interstitial_unit_id), requireActivity());
        interstitialAd.setListener(new MaxAdListener() {
            @Override
            public void onAdLoaded(MaxAd ad) {
                isInterstitialRequesting.set(false);
                interstitialAd.showAd();
            }

            @Override
            public void onAdDisplayed(MaxAd ad) {

            }

            @Override
            public void onAdHidden(MaxAd ad) {
                interstitialAd.destroy();
                interstitialAd = null;
            }

            @Override
            public void onAdClicked(MaxAd ad) {

            }

            @Override
            public void onAdLoadFailed(String adUnitId, MaxError error) {
                Toast.makeText(requireActivity(), "Error loading interstitial: " + error, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdDisplayFailed(MaxAd ad, MaxError error) {
                Toast.makeText(requireActivity(), "Error displaying interstitial: " + error, Toast.LENGTH_SHORT).show();
            }
        });
        interstitialAd.loadAd();
    }
}
