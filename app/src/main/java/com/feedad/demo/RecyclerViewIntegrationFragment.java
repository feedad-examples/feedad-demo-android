package com.feedad.demo;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.feedad.demo.ItemAdapter.ListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment that includes the video banner as an item inside a recycler view.
 * FeedAd will automatically start loading ads once the first ad view becomes visible.
 */
public class RecyclerViewIntegrationFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.title_recycler_view);
        return inflater.inflate(R.layout.feedad_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<ListItem> items = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            items.add(new ItemAdapter.ContentItem());
        }
        // add some ads to the content
        items.add(3, new ItemAdapter.FeedAdItem());
        items.add(8, new ItemAdapter.FeedAdItem());

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new ItemAdapter(items));
    }

}
