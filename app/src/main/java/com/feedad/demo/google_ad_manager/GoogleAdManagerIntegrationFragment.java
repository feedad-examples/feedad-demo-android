package com.feedad.demo.google_ad_manager;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.feedad.demo.R;
import com.feedad.demo.databinding.FeedadGoogleAdsBinding;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import com.google.android.gms.ads.admanager.AdManagerAdView;
import com.google.android.gms.ads.admanager.AdManagerInterstitialAd;
import com.google.android.gms.ads.admanager.AdManagerInterstitialAdLoadCallback;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;
import androidx.fragment.app.Fragment;

public class GoogleAdManagerIntegrationFragment extends Fragment {

    private static final String TAG = "GoogleAdManagerExample";

    private ObservableBoolean interstitialLoading;
    private ObservableBoolean googleAdSdkReady;

    @Nullable private AdManagerInterstitialAd currentInterstitialAd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        interstitialLoading = new ObservableBoolean(false);
        googleAdSdkReady = new ObservableBoolean(false);

        // enables ad request UI when Google Ads SDK is initialized
        MobileAds.initialize(requireContext(), initializationStatus -> googleAdSdkReady.set(true));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        @NonNull FeedadGoogleAdsBinding binding = FeedadGoogleAdsBinding.inflate(inflater, container, false);
        binding.setInterstitialRequesting(interstitialLoading);
        binding.setGoogleAdSDKReady(googleAdSdkReady);
        binding.btnRequestBanner.setOnClickListener(v -> requestBannerAd(binding));
        binding.btnRequestInterstitial.setOnClickListener(v -> requestInterstitialAd(binding));
        return binding.root;
    }

    private void requestBannerAd(@NonNull FeedadGoogleAdsBinding binding) {
        // Create a new ad view.
        AdManagerAdView adView = new AdManagerAdView(requireContext());
        adView.setAdSizes(getAdSize(binding.adContainer));
        adView.setAdUnitId(getString(R.string.google_ads_banner_unit_id));

        // Replace ad container with new ad view.
        binding.adContainer.removeAllViews();
        binding.adContainer.addView(adView);

        // Start loading the ad in the background.
        AdManagerAdRequest adRequest = new AdManagerAdRequest.Builder().build();
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                Log.w(TAG, "onAdFailedToLoad: " + loadAdError);
            }
        });
        adView.loadAd(adRequest);
    }

    private void requestInterstitialAd(@NonNull FeedadGoogleAdsBinding binding) {
        if (currentInterstitialAd == null) {
            AdManagerAdRequest adRequest = new AdManagerAdRequest.Builder().build();
            interstitialLoading.set(true);
            AdManagerInterstitialAd.load(requireContext(), getString(R.string.google_ads_interstitial_unit_id), adRequest,
                    new AdManagerInterstitialAdLoadCallback() {
                        @Override
                        public void onAdLoaded(@NonNull AdManagerInterstitialAd interstitialAd) {
                            Log.i(TAG, "onAdLoaded");
                            currentInterstitialAd = interstitialAd;
                            binding.btnRequestInterstitial.setText(R.string.show_interstitial_ad);
                            interstitialLoading.set(false);
                        }

                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            Log.d(TAG, loadAdError.toString());
                            interstitialLoading.set(false);
                        }
                    });
        } else {
            currentInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                @Override
                public void onAdClicked() {
                    // Called when a click is recorded for an ad.
                    Log.d(TAG, "Ad was clicked.");
                }

                @Override
                public void onAdDismissedFullScreenContent() {
                    // Called when ad is dismissed.
                    // Set the ad reference to null so you don't show the ad a second time.
                    Log.d(TAG, "Ad dismissed fullscreen content.");
                    currentInterstitialAd = null;
                    binding.btnRequestInterstitial.setText(R.string.request_interstitial_ad);
                }

                @Override
                public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                    // Called when ad fails to show.
                    Log.e(TAG, "Ad failed to show fullscreen content.");
                    currentInterstitialAd = null;
                    binding.btnRequestInterstitial.setText(R.string.request_interstitial_ad);
                }

                @Override
                public void onAdImpression() {
                    // Called when an impression is recorded for an ad.
                    Log.d(TAG, "Ad recorded an impression.");
                }

                @Override
                public void onAdShowedFullScreenContent() {
                    // Called when ad is shown.
                    Log.d(TAG, "Ad showed fullscreen content.");
                }
            });
            currentInterstitialAd.show(requireActivity());
        }
    }

    @NonNull
    private AdSize getAdSize(@NonNull View adContainerView) {
        // Determine the screen width (less decorations) to use for the ad width.
        Display display = requireActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = outMetrics.density;

        float adWidthPixels = adContainerView.getWidth();

        // If the ad hasn't been laid out, default to the full screen width.
        if (adWidthPixels == 0) {
            adWidthPixels = outMetrics.widthPixels;
        }

        int adWidth = (int) (adWidthPixels / density);
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(requireContext(), adWidth);
    }
}
