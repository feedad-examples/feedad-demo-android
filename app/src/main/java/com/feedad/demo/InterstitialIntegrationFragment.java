package com.feedad.demo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.feedad.android.FeedAd;
import com.feedad.android.FeedAdError;
import com.feedad.android.InterstitialAd;
import com.feedad.android.InterstitialAdPlaybackListener;
import com.feedad.android.InterstitialAdPresenter;
import com.feedad.android.InterstitialAdRequestListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * This class shows how to implement and request an interstitial ad
 */
public class InterstitialIntegrationFragment extends Fragment {

    private Button btnRequest;
    private Button btnShow;
    @Nullable private InterstitialAd interstitialAd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.feedad_interstitial, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnRequest = view.findViewById(R.id.btn_request);
        btnShow = view.findViewById(R.id.btn_show);

        prepareRequest();
    }

    /**
     * Prepares the UI to request an interstitial
     */
    private void prepareRequest() {
        btnRequest.setEnabled(true);
        btnRequest.setText(R.string.request_interstitial_ad);
        btnShow.setEnabled(false);
        btnRequest.setOnClickListener(view -> requestInterstitialAd());
    }

    /**
     * Requests an interstitial ad
     */
    private void requestInterstitialAd() {
        if (FeedAd.canRequestAd("interstitial-ad")) {
            interstitialAd = FeedAd.requestInterstitialAd(requireContext(), "interstitial-ad", new InterstitialAdRequestListener() {

                @Override
                public void onAdLoaded(InterstitialAdPresenter interstitialAdPresenter) {
                    prepareShow(interstitialAdPresenter);
                }

                @Override
                public void onError(FeedAdError feedAdError) {
                    // there was an error loading the ad or the placement had no fill
                    onInterstitialClosed("failed. Cause: " + feedAdError.toString());
                }
            });
            // set the UI to loading state
            btnRequest.setEnabled(false);
            btnRequest.setText(R.string.requesting_interstitial_ad);
        }
    }

    /**
     * Sets the UI state to show that the ad is ready to be displayed
     */
    private void prepareShow(final InterstitialAdPresenter interstitialAdDialog) {
        btnRequest.setEnabled(false);
        btnRequest.setText(R.string.interstitial_loaded);
        btnShow.setEnabled(true);
        btnShow.setOnClickListener(view -> {
            // Show the interstitial
            interstitialAdDialog.show(requireActivity(), new InterstitialAdPlaybackListener() {
                @Override
                public void onSkipped() {
                    // the ad was skippable and the user skipped it
                    onInterstitialClosed("skipped");
                }

                @Override
                public void onPlacementComplete() {
                    // the ad completed
                    onInterstitialClosed("completed");
                }

                @Override
                public void onError(FeedAdError feedAdError) {
                    // there was an error during ad playback
                    onInterstitialClosed("ad playback failed. Cause: " + feedAdError.toString());
                }

                @Override
                public void onCanceled() {
                    // the user canceled the ad by pressing the back button
                    onInterstitialClosed("canceled");
                }

                @Override
                public void onOpened() {
                    // the user clicked the ad. you can reward him by closing the ad or just do nothing and let it run until completion.
                    if (interstitialAd != null) {
                        interstitialAd.cancel(); // this will call the onCanceled callback.
                    }
                }
            }, false); // not cancelable by pressing back button
        });
    }

    /**
     * Shows why the interstitial was closed and resets the UI to load another interstitial
     */
    private void onInterstitialClosed(String reason) {
        Toast.makeText(getActivity(), "Interstitial " + reason, Toast.LENGTH_LONG).show();
        prepareRequest();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // When the view is destroyed while the interstitial is loading or shown it should be canceled
        if (interstitialAd != null) {
            interstitialAd.cancel();
        }
    }
}
