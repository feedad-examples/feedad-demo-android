package com.feedad.demo;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.applovin.sdk.AppLovinSdk;
import com.feedad.demo.applovin.AppLovinMAXIntegrationFragment;
import com.feedad.demo.google_ad_manager.GoogleAdManagerIntegrationFragment;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private View intro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        intro = findViewById(R.id.intro);

        initializeAppLovin();
    }

    private void initializeAppLovin() {
        AppLovinSdk.getInstance(this).setMediationProvider("max");
        AppLovinSdk.initializeSdk(this, config -> AppLovinSdk.getInstance(this).getSettings().setVerboseLogging(true));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        Fragment contentFragment = null;
        if (id == R.id.nav_scrollview) {
            contentFragment = new ScrollViewIntegrationFragment();
        } else if (id == R.id.nav_recyclerview) {
            contentFragment = new RecyclerViewIntegrationFragment();
        } else if (id == R.id.nav_recyclerview_advanced) {
            contentFragment = new AdvancedIntegrationFragment();
        } else if (id == R.id.nav_standalone) {
            contentFragment = new StandaloneIntegrationFragment();
        } else if (id == R.id.nav_interstitial) {
            contentFragment = new InterstitialIntegrationFragment();
        } else if (id == R.id.nav_applovin) {
            contentFragment = new AppLovinMAXIntegrationFragment();
        } else if (id == R.id.nav_google_ad_manager) {
            contentFragment = new GoogleAdManagerIntegrationFragment();
        }
        if (contentFragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, contentFragment)
                    .commit();
        }

        intro.setVisibility(View.GONE);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
