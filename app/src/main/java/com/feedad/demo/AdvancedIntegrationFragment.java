package com.feedad.demo;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.feedad.android.FeedAd;
import com.feedad.android.FeedAdConfig;
import com.feedad.android.FeedAdError;
import com.feedad.android.FeedAdListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * This fragment shows an advanced integration of FeedAds.
 * It starts showing content inside a recycler view,
 * requests an ad in the background, and adds it to the content once the ad is ready.
 */
public class AdvancedIntegrationFragment extends Fragment implements FeedAdListener {

    private static final String TAG = "AdvancedIntegration";

    private ItemAdapter itemAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // FeedAd supports setting custom information about the current user.
        // This can be set at any time and from any thread.
        FeedAd.setConfig(FeedAdConfig.newBuilder()
                                     .setUserAge(42)
                                     .setUserGender(FeedAdConfig.UserGender.Female)
                                     .setUserId("android-user-id")
                                     .build());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle(R.string.title_recyclerview_advanced);
        return inflater.inflate(R.layout.feedad_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // create a content of 10 items
        List<ItemAdapter.ListItem> items = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            items.add(new ItemAdapter.ContentItem());
        }

        // setup RV and adapter
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        itemAdapter = new ItemAdapter(items);
        recyclerView.setAdapter(itemAdapter);

        // add feed ad event listener
        FeedAd.addListener(this);
        String feedAdPlacementId = getString(R.string.feedad_placement_recyclerview);

        // request an ad if its not already active or requesting
        if (FeedAd.isActive(feedAdPlacementId)) {
            addPlacementItems();
        } else if (!FeedAd.isRequesting(feedAdPlacementId)) {
            FeedAd.requestAd(feedAdPlacementId);
        }
    }

    /**
     * Adds the ad item to the content
     */
    private void addPlacementItems() {
        itemAdapter.add(1, new ItemAdapter.FeedAdItem());
    }

    /**
     * Removes all ad placements
     */
    private void removePlacementItems() {
        itemAdapter.removeAllOfType(ItemAdapter.FeedAdItem.class);
    }

    @Override
    public void onDestroyView() {
        /* !!! any component that holds references to an activity has to be de-registered as listener during its destruction.
        * Otherwise the activity will be leaked. */
        FeedAd.removeListener(this);
        super.onDestroyView();
    }

    @Override
    public void onAdLoaded(@NonNull String placementId) {
        /* Called once the ad is ready */
        addPlacementItems();
    }

    @Override
    public void onPlacementComplete(@NonNull String placementId) {
        // ad playback was completed
        removePlacementItems();
    }

    @Override
    public void onOpened(@NonNull String placementId) {
        // the user clicked an ad
    }

    @Override
    public void onError(@Nullable String placementId, @NonNull FeedAdError error) {
        // there was an error playing the ad
        Log.w(TAG, String.format(Locale.ENGLISH, "FeedAd Error #%d: %s", error.getErrorCode(), error.getErrorMessage()));
        removePlacementItems();
    }
}
