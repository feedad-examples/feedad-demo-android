package com.feedad.demo;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This fragment demonstrates that FeedAdViews also work inside scroll views.
 * It is just added inside the scroll view's content and will request and play ads once it becomes visible.
 */
public class ScrollViewIntegrationFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.feedad_scrollview, container, false);
    }
}
