package com.feedad.demo;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.feedad.android.AdViewConfig;
import com.feedad.android.FeedAd;
import com.feedad.android.FeedAdError;
import com.feedad.android.StandaloneAd;
import com.feedad.android.StandaloneAdPlaybackListener;
import com.feedad.android.StandaloneAdRequestListener;
import com.feedad.android.StandaloneAdViewFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * This fragment shows the integration of a FeedAd Standalone Ad as a Pre-Roll ad.
 */
public class StandaloneIntegrationFragment extends Fragment {

    private ImageView player;
    private View btnPlay;
    private ViewGroup adContainer;
    private StandaloneAd standaloneAd;
    private ProgressBar progress;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.feedad_standalone, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        player = view.findViewById(R.id.player);
        btnPlay = view.findViewById(R.id.btn_play);
        adContainer = view.findViewById(R.id.ad_container);
        progress = view.findViewById(R.id.progress);

        btnPlay.setOnClickListener(view1 -> playAd());
    }

    private void playAd() {
        standaloneAd = FeedAd.requestStandaloneAd(requireContext(), "player-pre-roll", new StandaloneAdRequestListener() {

            @Override
            public <T extends View & AdViewConfig> void onAdLoaded(StandaloneAdViewFactory<T> standaloneAdViewFactory) {
                // Create the ad view for the current activity
                T adView = standaloneAdViewFactory.createStandaloneAdView(requireActivity(), new StandaloneAdPlaybackListener() {
                    @Override
                    public void onError(FeedAdError feedAdError) {
                        // an error occurred during ad playback
                        playContent();
                    }

                    @Override
                    public void onPlacementComplete() {
                        // the ad was completed
                        playContent();
                    }

                    @Override
                    public void onSkipped() {
                        // there was a skippable ad and the user skipped it
                        playContent();
                    }

                    @Override
                    public void onOpened() {
                        // the ad was clicked. We'll reward the user by stopping the ad and starting the content.
                        standaloneAd.cancel();
                        playContent();
                    }
                });

                //display a black screen while the ad is buffering
                adView.setShutterDrawable(new ColorDrawable(Color.BLACK));

                //add the ad to the container
                adContainer.addView(adView);

                //update visibility
                adContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(FeedAdError feedAdError) {
                // no ad or something went wrong? Display the content anyways:
                playContent();
            }
        });
        // display a progress indicator while the ad is loading
        progress.setVisibility(View.VISIBLE);
        btnPlay.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // if the view is destroyed while an ad is played it has to be destroyed.
        if (standaloneAd != null) {
            standaloneAd.cancel();
        }
    }

    /**
     * Pseudo-player that just displays an image for demo purposes.
     */
    private void playContent() {
        btnPlay.setVisibility(View.GONE);
        adContainer.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);
        player.setImageResource(R.drawable.test_pattern);
        player.setVisibility(View.VISIBLE);
    }
}
