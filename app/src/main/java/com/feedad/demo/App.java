package com.feedad.demo;

import com.feedad.android.FeedAd;
import com.feedad.android.FeedAdSdkOptions;

import androidx.multidex.MultiDexApplication;

public class App extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        // The SDK has to be initialized on application start
        FeedAd.init(
                this,
                getString(R.string.feedad_client_token),
                FeedAdSdkOptions.newBuilder()
                                .setEnableLogging(true)
                                .setWaitForConsent(false)
                                .build()
        );
    }
}
